﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public class SwitchShaderTimer : MonoBehaviour {
    public List<Shader> shaderList = new List<Shader>();
    public TextMesh fName;
    private Renderer rend;

    private int curShaderNr = 0;
    private float rangeInput = 0;

	// Use this for initialization
	void Start () {
        rend = GetComponent<Renderer>();

        rend.material.shader = shaderList[curShaderNr];

        rend.material.SetFloat("_RangeInput", rangeInput);

        fName.text = rend.material.shader.name.Split('/')[1];

        OVRTouchpad.Create();
        OVRTouchpad.TouchHandler += HandleTouchHandler;

    }

    // for controller
    //void Update()
    //{
    //    if (Input.GetButtonDown("Fire1"))
    //    {
    //        rangeInput = 0;
    //        curShaderNr--;
    //        if (curShaderNr < 0)
    //            curShaderNr = shaderList.Count - 1;
    //        rend.material.shader = shaderList[curShaderNr];
    //        rend.material.SetFloat("_RangeInput", rangeInput);
    //    }
    //    else if (Input.GetButtonDown("Jump"))
    //    {
    //        rangeInput = 0;
    //        curShaderNr++;
    //        if (curShaderNr >= shaderList.Count)
    //            curShaderNr = 0;
    //        rend.material.shader = shaderList[curShaderNr];
    //        rend.material.SetFloat("_RangeInput", rangeInput);
    //    }
    //    else if (Input.GetButtonDown("Fire3"))
    //    {
    //        rangeInput += 1f;
    //        if (rangeInput > 10)
    //            rangeInput = 0;
    //        rend.material.SetFloat("_RangeInput", rangeInput);
    //    }
    //    else if (Input.GetButtonDown("Fire2"))
    //    {
    //        rangeInput -= 1f;
    //        if (rangeInput < 0)
    //            rangeInput = 10;
    //        rend.material.SetFloat("_RangeInput", rangeInput);
    //    }
    //}

    // left-right change shader
    // up-down change range input
    private void HandleTouchHandler(object sender, EventArgs e)
    {
        var touchArgs = (OVRTouchpad.TouchArgs)e;
        OVRTouchpad.TouchEvent touchEvent = touchArgs.TouchType;
        switch (touchEvent)
        {
            case OVRTouchpad.TouchEvent.SingleTap:
                //Do something for Single Tap
                break;

            case OVRTouchpad.TouchEvent.Left:
                rangeInput = 0;
                curShaderNr--;
                if (curShaderNr < 0)
                    curShaderNr = shaderList.Count - 1;
                rend.material.shader = shaderList[curShaderNr];
                rend.material.SetFloat("_RangeInput", rangeInput);
                fName.text = rend.material.shader.name.Split('/')[1];
                break;

            case OVRTouchpad.TouchEvent.Right:
                rangeInput = 0;
                curShaderNr++;
                if (curShaderNr >= shaderList.Count)
                    curShaderNr = 0;
                rend.material.shader = shaderList[curShaderNr];
                rend.material.SetFloat("_RangeInput", rangeInput);
                fName.text = rend.material.shader.name.Split('/')[1];
                break;

            case OVRTouchpad.TouchEvent.Up:
                rangeInput += 1f;
                if (rangeInput > 10)
                    rangeInput = 0;
                rend.material.SetFloat("_RangeInput", rangeInput);
                break;

            case OVRTouchpad.TouchEvent.Down:
                rangeInput -= 1f;
                if (rangeInput < 0)
                    rangeInput = 10;
                rend.material.SetFloat("_RangeInput", rangeInput);
                break;
        }
            
    }
}
