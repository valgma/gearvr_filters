﻿Shader "Custom/Average" {
Properties{
	_MainTex("Base (RGB)", 2D) = "white" { }
	_RangeInput("Range Input", Float) = 0
}
SubShader{
	Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
	Pass{
		Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
		ZWrite Off
		Cull Off
		Stencil{
			Ref 250.000000
			Pass Replace
		}
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma target 3.0
		#include "UnityCG.cginc"

		// uniforms
		float4 _MainTex_ST;

		// vertex shader input data
		struct appdata {
			float3 pos : POSITION;
			float3 uv0 : TEXCOORD0;
		};

		// vertex-to-fragment interpolators
		struct v2f {
			fixed4 color : COLOR0;
			float2 uv0 : TEXCOORD0;
			float4 pos : SV_POSITION;
		};

		// vertex shader
		v2f vert(appdata IN) {
			v2f o;
			half4 color = half4(0,0,0,1.1);
			float3 eyePos = mul(UNITY_MATRIX_MV, float4(IN.pos,1)).xyz;
			half3 viewDir = 0.0;
			o.color = saturate(color);
			// compute texture coordinates
			o.uv0 = IN.uv0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
			// transform position
			o.pos = UnityObjectToClipPos(IN.pos);
			return o;
		}

		// textures
		sampler2D _MainTex;
		float4 _MainTex_TexelSize;
		float _TexHeight;
		float _TexWidth;
		float _RangeInput;

		// fragment shader
		float4 frag(v2f IN) : SV_Target{
			float4 col;
			int _KernelLength = (int)_RangeInput + 1;
			float kernelSizeX = _MainTex_TexelSize.x; // _ScreenParams.x;
			float kernelSizeY = _MainTex_TexelSize.y; // _ScreenParams.y;
			float w = 1.0f / (_KernelLength * _KernelLength);
			int r = (int)(_KernelLength / 2);
						
			col = float4(0, 0, 0, 0);
			for (int i = 0; i < _KernelLength; i++) {
				for (int j = 0; j < _KernelLength; j++) {
					col += tex2D(_MainTex, float2(IN.uv0.x + (i - r)*kernelSizeX, IN.uv0.y + (j - r)*kernelSizeY)) * w;
				}
			}

			return col;
		}


		ENDCG
	}
}
	Fallback "Diffuse"
}