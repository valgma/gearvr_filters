﻿Shader "Custom/Bloom2" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" { }
		_RangeInput("Range Input", Float) = 0
		_GrabPassPar("GrapPass parameters", Vector) = (53, 94, 192, 108)
	}
		SubShader{
		Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
		ZWrite Off
		Cull Off
		Stencil{
		Ref 250.000000
		Pass Replace
	}

		CGINCLUDE

#include "UnityCG.cginc"

	struct appdata_t {
		float4 vertex : POSITION;
		float2 texcoord: TEXCOORD0;
	};

	struct v2f {
		float4 vertex : POSITION;
		float4 uvgrab : TEXCOORD0;
		float2 uvmain : TEXCOORD2;
	};

	float4 _MainTex_ST;

	v2f vert(appdata_t v) {
		v2f o;
		o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
		#if UNITY_UV_STARTS_AT_TOP
		float scale = -1.0;
		#else
		float scale = 1.0;
		#endif
		o.uvgrab.xy = (float2(o.vertex.x, o.vertex.y*scale) + o.vertex.w) * 0.5;
		o.uvgrab.zw = o.vertex.zw;
		o.uvmain = TRANSFORM_TEX(v.texcoord, _MainTex);
		return o;
	}

	// textures
	sampler2D _MainTex;
	float _RangeInput;

	// fragment shader
	fixed4 frag_extract_bright(v2f IN) : SV_Target{
		fixed4 col = fixed4(0, 0, 0, 1);
		float thresh = 0.5 + _RangeInput / 20.0;

		fixed4 tmp = tex2D(_MainTex, IN.uvmain.xy);
		float brightness = dot(tmp.rgb, float3(0.2126, 0.7152, 0.0722));

		if (brightness > thresh)
			col = tmp;

		return col;
	}

	// for grabpass
	sampler2D _GrabTexture;
	float4 _MainTex_TexelSize;
	float4 _GrabPassPar;

	half4 frag_blur_h(v2f IN) : COLOR{

		float w = _GrabPassPar.z;

		half4 sum = half4(0,0,0,0);
		float texelsize_x = 5 * w / _MainTex_TexelSize.z;

#define GRABPIXEL(weight,kernelx) tex2Dproj( _GrabTexture, UNITY_PROJ_COORD(float4(IN.uvgrab.x + texelsize_x * kernelx, IN.uvgrab.y, IN.uvgrab.z, IN.uvgrab.w))) * weight

		sum += GRABPIXEL(0.05, -4.0);
		sum += GRABPIXEL(0.09, -3.0);
		sum += GRABPIXEL(0.12, -2.0);
		sum += GRABPIXEL(0.15, -1.0);
		sum += GRABPIXEL(0.18,  0.0);
		sum += GRABPIXEL(0.15, +1.0);
		sum += GRABPIXEL(0.12, +2.0);
		sum += GRABPIXEL(0.09, +3.0);
		sum += GRABPIXEL(0.05, +4.0);

		return sum;
	}

	half4 frag_blur_v(v2f IN) : COLOR{

		float h = _GrabPassPar.w;

		half4 sum = half4(0,0,0,0);
		float texelsize_y = 5 * h / _MainTex_TexelSize.w;

#define GRABPIXEL1(weight,kernely) tex2Dproj( _GrabTexture, UNITY_PROJ_COORD(float4(IN.uvgrab.x, IN.uvgrab.y + texelsize_y * kernely, IN.uvgrab.z, IN.uvgrab.w))) * weight

		sum += GRABPIXEL1(0.05, -4.0);
		sum += GRABPIXEL1(0.09, -3.0);
		sum += GRABPIXEL1(0.12, -2.0);
		sum += GRABPIXEL1(0.15, -1.0);
		sum += GRABPIXEL1(0.18,  0.0);
		sum += GRABPIXEL1(0.15, +1.0);
		sum += GRABPIXEL1(0.12, +2.0);
		sum += GRABPIXEL1(0.09, +3.0);
		sum += GRABPIXEL1(0.05, +4.0);

		return sum;
	}

	// add them together again
	fixed4 frag_add(v2f IN) : COLOR{

		fixed4 main_col = tex2D(_MainTex, IN.uvmain.xy);
		fixed4 bright_col = tex2Dproj(_GrabTexture, UNITY_PROJ_COORD(IN.uvgrab));

		return main_col + bright_col;
	}

		ENDCG

	Pass {
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag_extract_bright
		#pragma target 3.0
		ENDCG
	}
	GrabPass {}
	Pass {
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag_blur_h
		#pragma target 3.0
		ENDCG
	}
	GrabPass {}
	Pass {
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag_blur_v
		#pragma target 3.0
		ENDCG
	}
	GrabPass {}
	Pass {
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag_add
		#pragma target 3.0
		ENDCG
	}


	}
		Fallback "Diffuse"
}