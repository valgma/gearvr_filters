﻿Shader "Custom/DarkCinematicEffect" {
Properties{
	_MainTex("Base (RGB)", 2D) = "white" { }
}
SubShader{
	Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
	Pass{
		Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
		ZWrite Off
		Cull Off
		Stencil{
			Ref 250.000000
			Pass Replace
		}
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma target 3.0
#include "UnityCG.cginc"
#pragma multi_compile_fog




		///Functions block

fixed4 RGBtoHSV(in fixed4 RGB){// http://www.chilliant.com/rgb2hsv.html
	fixed4 k = fixed4(0.0, -1.0/3.0, 2.0/3.0, -1.0);
	fixed4 p = RGB.g < RGB.b ? fixed4(RGB.b, RGB.g, k.w, k.z) : fixed4(RGB.gb, k.xy);
	fixed4 q = RGB.r < p.x   ? fixed4(p.x, p.y, p.w, RGB.r) : fixed4(RGB.r, p.yzx);
	float d = q.x - min(q.w, q.y);
	float e = 1.0e-10;
	return fixed4(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x, RGB.a);
}

fixed4 HSVtoRGB(in fixed4 HSV){
	fixed4 k = fixed4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	fixed3 p = abs(frac(HSV.xxx + k.xyz) * 6.0 - k.www);
	return fixed4(HSV.z * lerp(k.xxx, clamp(p - k.xxx, 0.0, 1.0), HSV.y),HSV.a);
}

		//functions block end



#define USING_FOG (defined(FOG_LINEAR) || defined(FOG_EXP) || defined(FOG_EXP2))

		// uniforms
		float4 _MainTex_ST;

		// vertex shader input data
		struct appdata {
			float3 pos : POSITION;
			float3 uv0 : TEXCOORD0;
		};

		// vertex-to-fragment interpolators
		struct v2f {
			fixed4 color : COLOR0;
			float2 uv0 : TEXCOORD0;
#if USING_FOG
			fixed fog : TEXCOORD1;
#endif
			float4 pos : SV_POSITION;
		};

		// vertex shader
		v2f vert(appdata IN) {
			v2f o;
			half4 color = half4(0,0,0,1.1);
			float3 eyePos = mul(UNITY_MATRIX_MV, float4(IN.pos,1)).xyz;
			half3 viewDir = 0.0;
			o.color = saturate(color);
			// compute texture coordinates
			o.uv0 = IN.uv0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
			// fog
#if USING_FOG
			float fogCoord = length(eyePos.xyz); // radial fog distance
			UNITY_CALC_FOG_FACTOR(fogCoord);
			o.fog = saturate(unityFogFactor);
#endif
			// transform position
			o.pos = UnityObjectToClipPos(IN.pos);
			return o;
		}

		// textures
		sampler2D _MainTex;

		// fragment shader
		fixed4 frag(v2f IN) : SV_Target{
			fixed4 mainCol = fixed4(1, 0, 0, 1);
			fixed4 col;
			fixed4 tex, tmp0, tmp1, tmp2, tmp3;
			// SetTexture #0
			tex = tex2D(_MainTex, IN.uv0.xy);
			
			col = tex;

			//http://www.polishedpicture.com/free-actions/create-a-dark-cinematic-effect-in-photoshop/

			// linear filter
			float x_high = 0.95;
			float x_low = 0.05;
			float y_high = 1.0;
			float y_low = 0.0;
			float p = col.g;
			p = y_low + (p - x_low)*((y_high - y_low) / (x_high - x_low));
			col.g = p;


			//symetric curves
			// k < -1 == Sigmund curve
			// k > 0 == inverse Sigmund curve
			// for big k crube becomes a line
			float val = col.r;
			float k = -2.0;
			float valabs = abs(val * 2 - 1);
			p = k*valabs / (k - valabs + 1);
			if (val < 0.5) p = p*(-1);
			p = (p + 1) / 2;
			col.r = p;
			
			val = ((col.b + 0.05)*0.95) / 1.05;
			k = 1.0;
			valabs = abs(val * 2 - 1);
			p = k*valabs / (k - valabs + 1);
			if (val < 0.5) p = p*(-1);
			p = (p + 1) / 2;
			col.b = p;

			//RGB curve
			k = 0.7;
			float t = col.r;
			col.r = k*t / (k - t + 1);
			t = col.g;
			col.g = k*t / (k - t + 1);
			t = col.b;
			col.b = k*t / (k - t + 1);

			//brightness and contrast
			float alpha = 1.1;
			col.r = alpha*(col.r - 0.5) + 0.5;
			col.g = alpha*(col.g - 0.5) + 0.5;
			col.b = alpha*(col.b - 0.5) + 0.5;



			// fog
#if USING_FOG
			//col.rgb = lerp(unity_FogColor.rgb, col.rgb, IN.fog);
#endif
			return col;
		}

		// texenvs
		//! TexEnv0: 01010000 01010000 [_MainTex]
		ENDCG
	}
}


	Fallback "Diffuse"
}












