﻿Shader "Custom/Edges" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" { }
		_RangeInput("Range Input", Float) = 0
		_GrabPassPar("GrapPass parameters", Vector) = (53, 94, 192, 108)
	}
		SubShader{
		Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
		ZWrite Off
		Cull Off
		Stencil{
		Ref 250.000000
		Pass Replace
	}

		CGINCLUDE

#include "UnityCG.cginc"

	struct appdata_t {
		float4 vertex : POSITION;
		float2 texcoord: TEXCOORD0;
	};

	struct v2f {
		float4 vertex : POSITION;
		float4 uvgrab : TEXCOORD0;
		float2 uvmain : TEXCOORD2;
	};

	float4 _MainTex_ST;

	v2f vert(appdata_t v) {
		v2f o;
		o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
		#if UNITY_UV_STARTS_AT_TOP
		float scale = -1.0;
		#else
		float scale = 1.0;
		#endif
		o.uvgrab.xy = (float2(o.vertex.x, o.vertex.y*scale) + o.vertex.w) * 0.5;
		o.uvgrab.zw = o.vertex.zw;
		o.uvmain = TRANSFORM_TEX(v.texcoord, _MainTex);
		return o;
	}

	// textures
	sampler2D _MainTex;
	float _RangeInput;

	// fragment shader
	fixed4 frag_bw(v2f IN) : SV_Target{

		fixed4 tex = tex2D(_MainTex, IN.uvmain.xy);
		fixed i = 0.2989 * tex.r + 0.5870 * tex.g + 0.1140 * tex.b;  // black-white
		fixed4 col = fixed4(i, i, i, 1);

		return col;
	}

	// for grabpass
	sampler2D _GrabTexture;
	float4 _MainTex_TexelSize;
	float4 _GrabPassPar;

	float4 frag_sobel(v2f IN) : COLOR{

		float w = _GrabPassPar.z;
		float h = _GrabPassPar.w;

		//half4 sum = half4(0,0,0,0);
		float texelsize_x = w / _MainTex_TexelSize.z;
		float texelsize_y = h / _MainTex_TexelSize.w;

#define GRABPIXEL(kernelx,kernely) tex2Dproj( _GrabTexture, UNITY_PROJ_COORD(float4(IN.uvgrab.x + texelsize_x * kernelx, IN.uvgrab.y + texelsize_y*kernely, IN.uvgrab.z, IN.uvgrab.w))) 

		float4 col;

		float4 p1 = GRABPIXEL(-1, 1);
		float4 p2 = GRABPIXEL(0, 1);
		float4 p3 = GRABPIXEL(1, 1);
		float4 p4 = GRABPIXEL(-1, 0);
		float4 p5 = GRABPIXEL(0, 0);
		float4 p6 = GRABPIXEL(1, 0);
		float4 p7 = GRABPIXEL(-1, -1);
		float4 p8 = GRABPIXEL(0, -1);
		float4 p9 = GRABPIXEL(1, -1);

		float g = abs(p1.r + 2 * p2.r + p3.r - p7.r - 2 * p8.r - p9.r) + abs(p3.r + 2 * p6.r + p9.r - p1.r - 2 * p4.r - p7.r);
		if (g < 0.7)
			g = 0;
		else
			g = 1;
		
		int n = (int)_RangeInput;
		if (n==0)
			col = float4(g, g, g, 1);
		else if (n==1)
			col = float4(g, 0, 0, 1);
		else if (n == 2)
			col = float4(0, g, 0, 1);
		else if (n == 3)
			col = float4(0, 0, g, 1);
		else if (n == 4)
			col = float4(0, g, g, 1);
		else if (n == 5)
			col = float4(g, 0, g, 1);
		else if (n == 6)
			col = float4(g, g, 0, 1);
		else if (n == 7)
			col = float4(g, 0.5*g, 0.5*g, 1);
		else if (n == 8)
			col = float4(0.5*g, g, 0.5*g, 1);
		else if (n == 9)
			col = float4(0.5*g, 0.5*g, g, 1);
		else if (n == 10)
			col = float4(1-g, 1-g, 1-g, 1);
		else
			col = float4(g, g, g, 1);

		return col;
	}

		ENDCG

	Pass {
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag_bw
		#pragma target 3.0
		ENDCG
	}
	GrabPass {}
	Pass {
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag_sobel
		#pragma target 3.0
		ENDCG
	}


	}
		Fallback "Diffuse"
}