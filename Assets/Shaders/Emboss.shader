Shader "Custom/Emboss" {
Properties{
	_MainTex("Base (RGB)", 2D) = "white" { }
	_RangeInput("Range Input", Float) = 1
}
SubShader{
	Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
	Pass{
		Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
		ZWrite Off
		Cull Off
		Stencil{
			Ref 250.000000
			Pass Replace
		}
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma target 3.0
#include "UnityCG.cginc"






		// uniforms
		float4 _MainTex_ST;

		// vertex shader input data
		struct appdata {
			float3 pos : POSITION;
			float3 uv0 : TEXCOORD0;
		};

		// vertex-to-fragment interpolators
		struct v2f {
			fixed4 color : COLOR0;
			float2 uv0 : TEXCOORD0;
			float4 pos : SV_POSITION;
		};

		// vertex shader
		v2f vert(appdata IN) {
			v2f o;
			half4 color = half4(0,0,0,1.1);
			float3 eyePos = mul(UNITY_MATRIX_MV, float4(IN.pos,1)).xyz;
			half3 viewDir = 0.0;
			o.color = saturate(color);
			// compute texture coordinates
			o.uv0 = IN.uv0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
			// transform position
			o.pos = UnityObjectToClipPos(IN.pos);
			return o;
		}

		// textures
		sampler2D _MainTex;
		float _RangeInput;

		// fragment shader
		fixed4 frag(v2f IN) : SV_Target{
			fixed4 col;
			fixed4 tex;
			
			//Diagonal TopLeft to BotRight  Emboss filter and its application on the original image with varying sample sizes
			
			float dist = abs(200 * _RangeInput - 1000)+100;			//dist defines the 3x3 sample grid size and is dependant on the _RangeInput value

			for (int i = 0; i < 9; i++) {
				tex = tex2D(_MainTex, float2(IN.uv0.x + i % 3 / dist, IN.uv0.y + floor(i / 3) / dist));		//This selects the texels in a 3x3 grid around the current texel, the sampple size of the 3x3 grid varies based on the dist value
				
				if (i < 4 && i != 2) col.rgb -= tex.rgb/6;					//Indexes 0,1 and 3 get *-1/6 value added to the output
				
				else if (i > 4 && i != 6) col.rgb += tex.rgb / 6;		//Indexes 5,7 and 8 get *1/6 value added to the output
				
				else if (i == 4) {
					if (_RangeInput < 6) {			//For the input ranges 0 - 5, set the background to be gray like a traditional emboss filter
						col.rgb += 0.5;
					}
					else {
						col.rgb += tex.rgb/3;			//For the input ranges 6 - 10, add the emboss value on top of the original pixel value at that location
					}
				}
			}
			if (_RangeInput > 5) col.rgb *= 3;		//To increase brightness in the added emboss version, the output is multiplied by 3

			return col;
		}

		// texenvs
		//! TexEnv0: 01010000 01010000 [_MainTex]
		ENDCG
	}
}


	Fallback "Diffuse"
}












