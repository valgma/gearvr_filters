Shader "Custom/Colorblindness" {
		Properties{
			_MainTex("Base (RGB)", 2D) = "white" { }
			_RangeInput("Range Input", Float) = 1
		}
			SubShader{
			Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
			Pass{
			Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
			ZWrite Off
			Cull Off
			Stencil{
			Ref 250.000000
			Pass Replace
		}
			CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma target 3.0
#include "UnityCG.cginc"


		///Functions block


		fixed4 colorblind(in fixed4 RGB, in float3 red, in float3 green, in float3 blue) {//http://web.archive.org/web/20081014161121/http://www.colorjack.com/labs/colormatrix/

			float r = ((RGB.r*red.x) + (RGB.g*red.y) + (RGB.b*red.z));
			float g = ((RGB.r*green.x) + (RGB.g*green.y) + (RGB.b*green.z));
			float b = ((RGB.r*blue.x) + (RGB.g*blue.y) + (RGB.b*blue.z));
			return fixed4(r, g, b, 1);
		}




		fixed4 RGBtoHSV(in fixed4 RGB) {// http://www.chilliant.com/rgb2hsv.html
		fixed4 k = fixed4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
		fixed4 p = RGB.g < RGB.b ? fixed4(RGB.b, RGB.g, k.w, k.z) : fixed4(RGB.gb, k.xy);
		fixed4 q = RGB.r < p.x ? fixed4(p.x, p.y, p.w, RGB.r) : fixed4(RGB.r, p.yzx);
		float d = q.x - min(q.w, q.y);
		float e = 1.0e-10;
		return fixed4(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x, RGB.a);
	}
	fixed4 HSVtoRGB(in fixed4 HSV) {
		fixed4 k = fixed4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
		fixed3 p = abs(frac(HSV.xxx + k.xyz) * 6.0 - k.www);
		return fixed4(HSV.z * lerp(k.xxx, clamp(p - k.xxx, 0.0, 1.0), HSV.y),HSV.a);
	}

	//functions block end


	// uniforms
	float4 _MainTex_ST;

	// vertex shader input data
	struct appdata {
		float3 pos : POSITION;
		float3 uv0 : TEXCOORD0;
	};

	// vertex-to-fragment interpolators
	struct v2f {
		fixed4 color : COLOR0;
		float2 uv0 : TEXCOORD0;
		float4 pos : SV_POSITION;
	};

	// vertex shader
	v2f vert(appdata IN) {
		v2f o;
		half4 color = half4(0,0,0,1.1);
		float3 eyePos = mul(UNITY_MATRIX_MV, float4(IN.pos,1)).xyz;
		half3 viewDir = 0.0;
		o.color = saturate(color);
		// compute texture coordinates
		o.uv0 = IN.uv0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
		// transform position
		o.pos = UnityObjectToClipPos(IN.pos);
		return o;
	}

	// textures
	sampler2D _MainTex;
	float _RangeInput;

	// fragment shader
	fixed4 frag(v2f IN) : SV_Target{
	fixed4 col;
	fixed4 tex;
	// SetTexture #0
	tex = tex2D(_MainTex, IN.uv0.xy);
	int state = (int)_RangeInput;
	
	
	//Colorblindness filters based on the Javascript Color Matrix Library to simulate 4 main types of colorblindness along with their mild variants.
	
	
	if (0 == state) {
		col = colorblind(tex, float3(1, 0, 0), float3(0, 1, 0), float3(0, 0, 1));//Normal
	}
	else if (1 == state) {
		col = colorblind(tex, float3(0.618, 0.320, 0.062), float3(0.163, 0.775, 0.062), float3(0.163, 0.320, 0.516));//Achromatomaly
	}
	else if (2 == state) {
		col = colorblind(tex, float3(0.299, 0.587, 0.114), float3(0.299, 0.587, 0.114), float3(0.299, 0.587, 0.114));//Achromatopsia
	}
	else if (3 == state) {
		col = colorblind(tex, float3(0.8, 0.2, 0), float3(0.258, 0.742, 0), float3(0, 0.142, 0.858));//Deuteranomaly
		col = RGBtoHSV(col);			//Fix for Deutranomalys and Deutranopias hue shift
		col.x-= 0.05;
		if (col.x<0) col.x+=1;
		col = HSVtoRGB(col);
	}
	else if (4 == state) {
		col = colorblind(tex, float3(0.625, 0.375, 0), float3(0.7, 0.3, 0), float3(0, 0.3, 0.7));//Deuteranopia
		col = RGBtoHSV(col);			//Fix for Deutranomalys and Deutranopias hue shift
		col.x-= 0.05;
		if (col.x<0) col.x+=1;
		col = HSVtoRGB(col);
	}
	else if (5 == state) {
		col = colorblind(tex, float3(0.817, 0.183, 0), float3(0.333, 0.667, 0), float3(0, 0.125, 0.875));//Protanomaly
	}
	else if (6 == state) {
		col = colorblind(tex, float3(0.567, 0.433, 0), float3(0.558, 0.442, 0), float3(0, 0.242, 0.758));//Protanopia
	}
	else if (7 == state) {
		col = colorblind(tex, float3(0.967, 0.033, 0), float3(0, 0.733, 0.267), float3(0, 0.183, 0.817));//Tritanomaly
	}
	else if (8 == state) {
		col = colorblind(tex, float3(0.95, 0.05, 0), float3(0, 0.433, 0.567), float3(0, 0.475, 0.525));//Tritanopia
	}
	else {
		col = fixed4(0,0,0,1);
	}



	return col;
	}

		// texenvs
		//! TexEnv0: 01010000 01010000 [_MainTex]
		ENDCG
	}
	}


		Fallback "Diffuse"
}




//return col;