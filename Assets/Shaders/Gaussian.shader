﻿Shader "Custom/Gaussian" {
//https://forum.unity3d.com/threads/simple-optimized-blur-shader.185327/
Properties{
	_MainTex("Base (RGB)", 2D) = "white" { }
	_RangeInput("Range Input", Float) = 0
	_GrabPassPar("GrapPass parameters", Vector) = (53, 94, 192, 108)
}
SubShader{
	Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
	ZWrite Off
	Cull Off
	Stencil{
		Ref 250.000000
		Pass Replace
	}

	CGINCLUDE

	#include "UnityCG.cginc"

	// uniforms
	float4 _MainTex_ST;

	// vertex shader input data
	struct appdata {
		float3 pos : POSITION;
		float3 uv0 : TEXCOORD0;
	};

	// vertex-to-fragment interpolators
	struct v2f {
		fixed4 color : COLOR0;
		float2 uv0 : TEXCOORD0;
		float4 pos : SV_POSITION;
	};

	// vertex shader
	v2f vert(appdata IN) {
		v2f o;
		half4 color = half4(0,0,0,1.1);
		float3 eyePos = mul(UNITY_MATRIX_MV, float4(IN.pos,1)).xyz;
		half3 viewDir = 0.0;
		o.color = saturate(color);
		// compute texture coordinates
		o.uv0 = IN.uv0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
		// transform position
		o.pos = UnityObjectToClipPos(IN.pos);
		return o;
	}

	// textures
	sampler2D _MainTex;
	float _RangeInput;

	// fragment shader
	fixed4 frag(v2f IN) : SV_Target{
		fixed4 col;

		fixed4 tmp = tex2D(_MainTex, IN.uv0.xy);

		col = tmp;

		return col;
	}

	// for grabpass
	struct appdata_t {
		float4 vertex : POSITION;
		float2 texcoord: TEXCOORD0;
	};

	struct v2f_gp {
		float4 vertex : POSITION;
		float4 uvgrab : TEXCOORD0;
	};

	v2f_gp vert_gp(appdata_t v) {
		v2f_gp o;
		o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
		#if UNITY_UV_STARTS_AT_TOP
		float scale = -1.0;
		#else
		float scale = 1.0;
		#endif
		o.uvgrab.xy = (float2(o.vertex.x, o.vertex.y*scale) + o.vertex.w) * 0.5;
		o.uvgrab.zw = o.vertex.zw;
		return o;
	}

	sampler2D _GrabTexture;
	float4 _MainTex_TexelSize;
	float4 _GrabPassPar;

	half4 frag_h(v2f_gp i) : COLOR{

		float w = _GrabPassPar.z;

		half4 sum = half4(0,0,0,0);
		float texelsize_x = (1 + 0.5*_RangeInput) * w / _MainTex_TexelSize.z;

#define GRABPIXEL(weight,kernelx) tex2Dproj( _GrabTexture, UNITY_PROJ_COORD(float4(i.uvgrab.x + texelsize_x * kernelx, i.uvgrab.y, i.uvgrab.z, i.uvgrab.w))) * weight

		sum += GRABPIXEL(0.05, -4.0);
		sum += GRABPIXEL(0.09, -3.0);
		sum += GRABPIXEL(0.12, -2.0);
		sum += GRABPIXEL(0.15, -1.0);
		sum += GRABPIXEL(0.18,  0.0);
		sum += GRABPIXEL(0.15, +1.0);
		sum += GRABPIXEL(0.12, +2.0);
		sum += GRABPIXEL(0.09, +3.0);
		sum += GRABPIXEL(0.05, +4.0);

		return sum;
	}

	half4 frag_v(v2f_gp i) : COLOR{

		float h = _GrabPassPar.w;

		half4 sum = half4(0,0,0,0);
		float texelsize_y = (1 + 0.5*_RangeInput) * h / _MainTex_TexelSize.w;

#define GRABPIXEL1(weight,kernely) tex2Dproj( _GrabTexture, UNITY_PROJ_COORD(float4(i.uvgrab.x, i.uvgrab.y + texelsize_y * kernely, i.uvgrab.z, i.uvgrab.w))) * weight

		//G(X) = (1/(sqrt(2*PI*deviation*deviation))) * exp(-(x*x / (2*deviation*deviation)))

		sum += GRABPIXEL1(0.05, -4.0);
		sum += GRABPIXEL1(0.09, -3.0);
		sum += GRABPIXEL1(0.12, -2.0);
		sum += GRABPIXEL1(0.15, -1.0);
		sum += GRABPIXEL1(0.18,  0.0);
		sum += GRABPIXEL1(0.15, +1.0);
		sum += GRABPIXEL1(0.12, +2.0);
		sum += GRABPIXEL1(0.09, +3.0);
		sum += GRABPIXEL1(0.05, +4.0);

		return sum;
	}

		
	ENDCG

	Pass {
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma target 3.0
		ENDCG
	}
	GrabPass {}
	Pass {
		CGPROGRAM
		#pragma vertex vert_gp
		#pragma fragment frag_h
		#pragma target 3.0
		ENDCG
	}
	GrabPass {}
	Pass {
		CGPROGRAM
		#pragma vertex vert_gp
		#pragma fragment frag_v
		#pragma target 3.0
		ENDCG
	}
	
	
}
	Fallback "Diffuse"
}