﻿Shader "Custom/Hudson" {
Properties{
	_MainTex("Base (RGB)", 2D) = "white" { }
	_RangeInput("Range Input", Float) = 1
}
SubShader{
	Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
	Pass{
		Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
		ZWrite Off
		Cull Off
		Stencil{
			Ref 250.000000
			Pass Replace
		}
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma target 3.0
		#include "UnityCG.cginc"

		// uniforms
		float4 _MainTex_ST;

		// vertex shader input data
		struct appdata {
			float3 pos : POSITION;
			float3 uv0 : TEXCOORD0;
		};

		// vertex-to-fragment interpolators
		struct v2f {
			fixed4 color : COLOR0;
			float2 uv0 : TEXCOORD0;
			float4 pos : SV_POSITION;
		};

		// vertex shader
		v2f vert(appdata IN) {
			v2f o;
			half4 color = half4(0,0,0,1.1);
			float3 eyePos = mul(UNITY_MATRIX_MV, float4(IN.pos,1)).xyz;
			half3 viewDir = 0.0;
			o.color = saturate(color);
			// compute texture coordinates
			o.uv0 = IN.uv0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
			// transform position
			o.pos = UnityObjectToClipPos(IN.pos);
			return o;
		}

		// textures
		sampler2D _MainTex;
		float _RangeInput;

		// fragment shader
		fixed4 frag(v2f IN) : SV_Target{
			
			fixed4 col = tex2D(_MainTex, IN.uv0.xy);
			col.r *= 0.9;
			col.g *= 0.9;
			col.b *= 1 + _RangeInput / 10;
			col = saturate(col);

			float d = distance(IN.uv0.xy, float2(0.5, 0.5));

			if (d > 0.5)
				col *= 0.5 / d;
			
			return col;
		}

		// texenvs
		//! TexEnv0: 01010000 01010000 [_MainTex]
		ENDCG
	}
}


	Fallback "Diffuse"
}












