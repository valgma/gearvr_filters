Shader "Custom/hueRAINBOW" {
Properties{
	_MainTex("Base (RGB)", 2D) = "white" { }
	_RangeInput("Range Input", Float) = 1
}
SubShader{
	Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
	Pass{
		Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
		ZWrite Off
		Cull Off
		Stencil{
			Ref 250.000000
			Pass Replace
		}
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma target 3.0
#include "UnityCG.cginc"




		///Functions block

fixed4 RGBtoHSV(in fixed4 RGB){// http://www.chilliant.com/rgb2hsv.html
	fixed4 k = fixed4(0.0, -1.0/3.0, 2.0/3.0, -1.0);
	fixed4 p = RGB.g < RGB.b ? fixed4(RGB.b, RGB.g, k.w, k.z) : fixed4(RGB.gb, k.xy);
	fixed4 q = RGB.r < p.x   ? fixed4(p.x, p.y, p.w, RGB.r) : fixed4(RGB.r, p.yzx);
	float d = q.x - min(q.w, q.y);
	float e = 1.0e-10;
	return fixed4(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x, RGB.a);
}

fixed4 HSVtoRGB(in fixed4 HSV){
	fixed4 k = fixed4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	fixed3 p = abs(frac(HSV.xxx + k.xyz) * 6.0 - k.www);
	return fixed4(HSV.z * lerp(k.xxx, clamp(p - k.xxx, 0.0, 1.0), HSV.y),HSV.a);
}
		//functions block end


		// uniforms
		float4 _MainTex_ST;

		// vertex shader input data
		struct appdata {
			float3 pos : POSITION;
			float3 uv0 : TEXCOORD0;
		};

		// vertex-to-fragment interpolators
		struct v2f {
			fixed4 color : COLOR0;
			float2 uv0 : TEXCOORD0;
			float4 pos : SV_POSITION;
		};

		// vertex shader
		v2f vert(appdata IN) {
			v2f o;
			half4 color = half4(0,0,0,1.1);
			float3 eyePos = mul(UNITY_MATRIX_MV, float4(IN.pos,1)).xyz;
			half3 viewDir = 0.0;
			o.color = saturate(color);
			// compute texture coordinates
			o.uv0 = IN.uv0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
			// transform position
			o.pos = UnityObjectToClipPos(IN.pos);
			return o;
		}

		// textures
		sampler2D _MainTex;
		float _RangeInput;
		
		// fragment shader
		fixed4 frag(v2f IN) : SV_Target{
			fixed4 col;
			fixed4 tex;
			// SetTexture #0
			tex = tex2D(_MainTex, IN.uv0.xy);
			
			//Simple Hue change based filters

			fixed4 hsv = RGBtoHSV(tex);		//Convert to HSV
			float t = (_Time.y%2)/2;			//_Time = ( t/20, t, t*2, t*3 ) so _Time.y = t      for more about time -> https://docs.unity3d.com/462/Documentation/Manual/SL-BuiltinValues.html
			
			if ((int)_RangeInput%3==0) hsv.x = hsv.x+ t + IN.uv0.x;	//Shift hue ( hsv = (hue, sat, val) ) by adding time and the x position
			else if ((int)_RangeInput%3==1) hsv.x = hsv.x+ t;	//Hue shift variant without the texel location dependancie
			else hsv.x = t;	//Hue shift variant that binds the entire hue to one value
			
			
			while (hsv.x > 1) hsv.x -= 1;	//set hue range back to (0, 1)
			col = HSVtoRGB(hsv);			//Convert to RGB


			return col;
		}

		// texenvs
		//! TexEnv0: 01010000 01010000 [_MainTex]
		ENDCG
	}
}


	Fallback "Diffuse"
}












