﻿Shader "Custom/LightDarkIntense" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" { }
		_RangeInput("Range Input", Float) = 0
	}
		SubShader{
		Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
		Pass{
		Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
		ZWrite Off
		Cull Off
		Stencil{
		Ref 250.000000
		Pass Replace
	}
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma target 3.0
#include "UnityCG.cginc"
#pragma multi_compile_fog

		fixed4 RGBtoHSV(in fixed4 RGB) {
		fixed4 k = fixed4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
		fixed4 p = RGB.g < RGB.b ? fixed4(RGB.b, RGB.g, k.w, k.z) : fixed4(RGB.gb, k.xy);
		fixed4 q = RGB.r < p.x ? fixed4(p.x, p.y, p.w, RGB.r) : fixed4(RGB.r, p.yzx);
		float d = q.x - min(q.w, q.y);
		float e = 1.0e-10;
		return fixed4(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x, RGB.a);
	}

	fixed4 HSVtoRGB(in fixed4 HSV) {
		fixed4 k = fixed4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
		fixed3 p = abs(frac(HSV.xxx + k.xyz) * 6.0 - k.www);
		return fixed4(HSV.z * lerp(k.xxx, clamp(p - k.xxx, 0.0, 1.0), HSV.y),HSV.a);
	}

#define USING_FOG (defined(FOG_LINEAR) || defined(FOG_EXP) || defined(FOG_EXP2))

	// uniforms
	float4 _MainTex_ST;

	// vertex shader input data
	struct appdata {
		float3 pos : POSITION;
		float3 uv0 : TEXCOORD0;
	};

	// vertex-to-fragment interpolators
	struct v2f {
		fixed4 color : COLOR0;
		float2 uv0 : TEXCOORD0;
#if USING_FOG
		fixed fog : TEXCOORD1;
#endif
		float4 pos : SV_POSITION;
	};

	// vertex shader
	v2f vert(appdata IN) {
		v2f o;
		half4 color = half4(0,0,0,1.1);
		float3 eyePos = mul(UNITY_MATRIX_MV, float4(IN.pos,1)).xyz;
		half3 viewDir = 0.0;
		o.color = saturate(color);
		// compute texture coordinates
		o.uv0 = IN.uv0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
		// fog
#if USING_FOG
		float fogCoord = length(eyePos.xyz); // radial fog distance
		UNITY_CALC_FOG_FACTOR(fogCoord);
		o.fog = saturate(unityFogFactor);
#endif
		// transform position
		o.pos = UnityObjectToClipPos(IN.pos);
		return o;
	}

	// textures
	sampler2D _MainTex;
	float _RangeInput;

	// fragment shader
	fixed4 frag(v2f IN) : SV_Target{
		fixed4 mainCol = fixed4(1, 0, 0, 1);
	fixed4 col;
	fixed4 tex, tmp0, tmp1, tmp2, tmp3;
	tex = tex2D(_MainTex, IN.uv0.xy);
	fixed4 hsv = RGBtoHSV(tex);

	float val = hsv.z;
	float k = (((10.001 - _RangeInput) / 50) + 1.00)*(-1);//_RangeInput 0-10

	float valabs = abs(val * 2 - 1);
	float p = k*valabs / (k - valabs + 1);
	if (val < 0.5) p = p*(-1);
	p = (p + 1) / 2;
	hsv.z = p;

	col = HSVtoRGB(hsv);



#if USING_FOG
	//col.rgb = lerp(unity_FogColor.rgb, col.rgb, IN.fog);
#endif
	return col;
	}

		// texenvs
		//! TexEnv0: 01010000 01010000 [_MainTex]
		ENDCG
	}
	}
		Fallback "Diffuse"
}

