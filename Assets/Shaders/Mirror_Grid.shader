Shader "Custom/Mirror_grid" {
Properties{
	_MainTex("Base (RGB)", 2D) = "white" { }
	_RangeInput("Range Input", Float) = 1
}
SubShader{
	Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
	Pass{
		Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
		ZWrite Off
		Cull Off
		Stencil{
			Ref 250.000000
			Pass Replace
		}
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma target 3.0
#include "UnityCG.cginc"




		// uniforms
		float4 _MainTex_ST;

		// vertex shader input data
		struct appdata {
			float3 pos : POSITION;
			float3 uv0 : TEXCOORD0;
		};

		// vertex-to-fragment interpolators
		struct v2f {
			fixed4 color : COLOR0;
			float2 uv0 : TEXCOORD0;
			float4 pos : SV_POSITION;
		};

		// vertex shader
		v2f vert(appdata IN) {
			v2f o;
			half4 color = half4(0,0,0,1.1);
			float3 eyePos = mul(UNITY_MATRIX_MV, float4(IN.pos,1)).xyz;
			half3 viewDir = 0.0;
			o.color = saturate(color);
			// compute texture coordinates
			o.uv0 = IN.uv0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
			// transform position
			o.pos = UnityObjectToClipPos(IN.pos);
			return o;
		}

		// textures
		sampler2D _MainTex;
		float _RangeInput;
		// fragment shader
		fixed4 frag(v2f IN) : SV_Target{
			fixed4 col;

			//A filter that splits the view area into grid sections where every other section in a direction has its axis flipped in that direction
			
			float step = 4*_RangeInput;		//step defines the amount of grid sections in the image
			float2 dist;
			float2 coords = float2(IN.uv0.x*0.90, IN.uv0.y*0.90);	//To remove the black borders on the right and bottom edges, 10% of those edges get cut out;
			for (int i = 0; i < step; i++) {
				dist = float2(coords[0]*step - (float)i, coords[1]*step - (float)i );	//dist defines a texels distance in its closest grid section

				if (dist[0] < 1 && dist[0] > 0 && i % 2 == 0) {								//for all even sections in the x axis, the x axis gets flipped
					coords[0] = ((float)(i)-dist[0] + 1.0) / step;
				}
				else if (dist[1] < 1 && dist[1] > 0 && i % 2 == 1) {						//for all even sections in the y axis, the y axis gets flipped
					coords[1] = ((float)(i)-dist[1] + 1.0) / step;
				}
				
			}
			col = tex2D(_MainTex, coords);

			
			
			return col;
		}

		// texenvs
		//! TexEnv0: 01010000 01010000 [_MainTex]
		ENDCG
	}
}


	Fallback "Diffuse"
}












