﻿Shader "Custom/MyImageEffect" {
Properties{
	_MainTex("Base (RGB)", 2D) = "white" { }
	_RangeInput("Range Input", Float) = 0
}
SubShader{
	Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
	Pass{
		Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
		ZWrite Off
		Cull Off
		Stencil{
			Ref 250.000000
			Pass Replace
		}
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma target 3.0
		#include "UnityCG.cginc"

		// uniforms
		float4 _MainTex_ST;

		// vertex shader input data
		struct appdata {
			float3 pos : POSITION;
			float3 uv0 : TEXCOORD0;
		};

		// vertex-to-fragment interpolators
		struct v2f {
			fixed4 color : COLOR0;
			float2 uv0 : TEXCOORD0;
			float4 pos : SV_POSITION;
		};

		// vertex shader
		v2f vert(appdata IN) {
			v2f o;
			half4 color = half4(0,0,0,1.1);
			float3 eyePos = mul(UNITY_MATRIX_MV, float4(IN.pos,1)).xyz;
			half3 viewDir = 0.0;
			o.color = saturate(color);
			// compute texture coordinates
			o.uv0 = IN.uv0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
			// transform position
			o.pos = UnityObjectToClipPos(IN.pos);
			return o;
		}

		// textures
		sampler2D _MainTex;
		float _RangeInput;

		// fragment shader
		fixed4 frag(v2f IN) : SV_Target{
			fixed4 mainCol;
			fixed4 col;
			fixed4 tex = tex2D(_MainTex, IN.uv0.xy);

			int n = (int)_RangeInput;
			if (n == 0)
				mainCol = fixed4(1, 0.41, 0.71, 1);  //pink
			else if (n == 1)
				mainCol = fixed4(1, 0, 0, 1);
			else if (n == 2)
				mainCol = fixed4(0, 1, 0, 1);
			else if (n == 3)
				mainCol = fixed4(0, 0, 1, 1);
			else if (n == 4)
				mainCol = fixed4(0, 1, 1, 1);
			else if (n == 5)
				mainCol = fixed4(1, 0, 1, 1);
			else if (n == 6)
				mainCol = fixed4(1, 1, 0, 1);
			else if (n == 7)
				mainCol = fixed4(1, 0.5, 0.5, 1);
			else if (n == 8)
				mainCol = fixed4(0.5, 1, 0.5, 1);
			else if (n == 9)
				mainCol = fixed4(0.5, 0.5, 1, 1);
			else if (n == 10)
				mainCol = fixed4(0.5, 0.5, 0.5, 1);
			else
				mainCol = fixed4(1, 1, 1, 1);

			col = lerp(tex, mainCol, 0.5);  // weighted linear combination
			
			return col;
		}

		// texenvs
		//! TexEnv0: 01010000 01010000 [_MainTex]
		ENDCG
	}
}
	Fallback "Diffuse"
}