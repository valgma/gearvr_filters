Shader "Custom/RGBshift" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" { }
		_RangeInput("Range Input", Float) = 1
	}
		SubShader{
		Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
		Pass{
		Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
		ZWrite Off
		Cull Off
		Stencil{
		Ref 250.000000
		Pass Replace
	}
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma target 3.0
#include "UnityCG.cginc"



	// uniforms
	float4 _MainTex_ST;

	// vertex shader input data
	struct appdata {
		float3 pos : POSITION;
		float3 uv0 : TEXCOORD0;
	};

	// vertex-to-fragment interpolators
	struct v2f {
		fixed4 color : COLOR0;
		float2 uv0 : TEXCOORD0;

		float4 pos : SV_POSITION;
	};

	// vertex shader
	v2f vert(appdata IN) {
		v2f o;
		half4 color = half4(0,0,0,1.1);
		float3 eyePos = mul(UNITY_MATRIX_MV, float4(IN.pos,1)).xyz;
		half3 viewDir = 0.0;
		o.color = saturate(color);
		// compute texture coordinates
		o.uv0 = IN.uv0.xy * _MainTex_ST.xy + _MainTex_ST.zw;

		// transform position
		o.pos = UnityObjectToClipPos(IN.pos);
		return o;
	}

	// textures
	sampler2D _MainTex;
	float _RangeInput;

	// fragment shader
	fixed4 frag(v2f IN) : SV_Target{
	fixed4 col;
	fixed4 tex;
	
	
	//A filter shifts and orbits the image location for each color value seperately

	float tc = _CosTime.w;
	float ts = _SinTime.w;
	float size = 0.003*_RangeInput;	//Defines the radius of the orbit

	fixed4 texR = tex2D(_MainTex, float2(clamp(IN.uv0.x + size*tc , 0, 1), clamp(IN.uv0.y + size*ts, 0, 1)));		//Each color value gets a seperate orbit
	fixed4 texG = tex2D(_MainTex, float2(clamp(IN.uv0.x + size*ts , 0, 1), clamp(IN.uv0.y - size*tc, 0, 1)));
	fixed4 texB = tex2D(_MainTex, float2(clamp(IN.uv0.x - size*tc , 0, 1), clamp(IN.uv0.y + size*ts, 0, 1)));
	
	col = fixed4(texR.r, texG.g, texB.b, texR.a);


	return col;
	}

		// texenvs
		//! TexEnv0: 01010000 01010000 [_MainTex]
		ENDCG
	}
	}


		Fallback "Diffuse"
}


