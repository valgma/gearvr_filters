Shader "Custom/Rotater" {
Properties{
	_MainTex("Base (RGB)", 2D) = "white" { }
	_RangeInput("Range Input", Float) = 1
}
SubShader{
	Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
	Pass{
		Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
		ZWrite Off
		Cull Off
		Stencil{
			Ref 250.000000
			Pass Replace
		}
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma target 3.0
#include "UnityCG.cginc"




		// uniforms
		float4 _MainTex_ST;

		// vertex shader input data
		struct appdata {
			float3 pos : POSITION;
			float3 uv0 : TEXCOORD0;
		};

		// vertex-to-fragment interpolators
		struct v2f {
			fixed4 color : COLOR0;
			float2 uv0 : TEXCOORD0;
			float4 pos : SV_POSITION;
		};

		// vertex shader
		v2f vert(appdata IN) {
			v2f o;
			half4 color = half4(0,0,0,1.1);
			float3 eyePos = mul(UNITY_MATRIX_MV, float4(IN.pos,1)).xyz;
			half3 viewDir = 0.0;
			o.color = saturate(color);
			// compute texture coordinates
			o.uv0 = IN.uv0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
			// transform position
			o.pos = UnityObjectToClipPos(IN.pos);
			return o;
		}

		// textures
		sampler2D _MainTex;
		float _RangeInput;

		// fragment shader
		fixed4 frag(v2f IN) : SV_Target{
			fixed4 col;
			fixed4 tex;
			float pi = 3.14159;
			float t;
			float2 midpoint = { 0.4, 0.5 };
			float xc;
			float yc;


			//A filter that rotates and streches the image in different ways with different time functions
			
			//Thse functions define the time value t for each sub filter
			if (_RangeInput == 0) t = (_Time.x % (2 * pi));
			if (_RangeInput == 1 || _RangeInput == 4 || _RangeInput == 6 || _RangeInput > 7) t = -(_Time.x % (2 * pi))*2;
			if (_RangeInput == 2 || _RangeInput == 5 || _RangeInput == 7) t = sin((_Time.x % (2 * pi)))*pi*2;
			if (_RangeInput == 3) t = cos((_Time.x % (2 * pi)))*pi;
			
			
			if (_RangeInput < 4) {	//Filters 0-3 rotate the image
				xc = ((IN.uv0.x - midpoint[0])*cos(t) / 1.5 - (IN.uv0.y - midpoint[1])*sin(t) / 1.5) + midpoint[0];
				yc = ((IN.uv0.y - midpoint[1])*cos(t) / 1.5 + (IN.uv0.x - midpoint[0])*sin(t) / 1.5) + midpoint[1];
			}
			else if (_RangeInput < 6) {	//Filters 4-5 strech the image
				xc = ((IN.uv0.x - midpoint[0])*cos(t) ) / 1.5 + midpoint[0];
				yc = ((IN.uv0.y - midpoint[1])*sin(t)) / 1.5 + midpoint[1];
				if (_RangeInput == 5) {
					xc += (IN.uv0.y - midpoint[1])*sin(t) / 1.5;	//Filter 5 gets a diagonal strech
					yc += (IN.uv0.y - midpoint[1])*cos(t) / 1.5;
				}
			}
			
			else if (_RangeInput < 8) {	//Filters 6-7 make the image orbit in circles
				xc = (0.1 * cos(t*8) + IN.uv0.x)*0.8+0.1;
				yc = (0.1 * sin(t*8) + IN.uv0.y)*0.8+0.1;
			}
			else if (_RangeInput < 9) {	//Filter 8 creates streching waves across the images
				xc = 0.05 * cos(t*2 + IN.uv0.x * 20) + IN.uv0.x *0.8+0.1;
				yc = 0.05 * cos(t*2 + IN.uv0.y * 20) + IN.uv0.y *0.8 + 0.1;
			}
			else if (_RangeInput < 11) {	//Filters 9-10 create wave distortions on the image
				xc =  0.04 * cos(t*2 + IN.uv0.y * 20) + IN.uv0.x *0.8 + 0.1;
				yc = IN.uv0.y;
				if (_RangeInput == 10) {
					yc = 0.04 * cos(t*2 + IN.uv0.x * 20) + IN.uv0.y *0.8 + 0.1;
				}
			}







			col = tex2D(_MainTex, float2(xc, yc));

			return col;
		}

		// texenvs
		//! TexEnv0: 01010000 01010000 [_MainTex]
		ENDCG
	}
}


	Fallback "Diffuse"
}












