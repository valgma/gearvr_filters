﻿Shader "Custom/Sepia" {
Properties{
	_MainTex("Base (RGB)", 2D) = "white" { }
	_RangeInput("Range Input", Float) = 0
}
SubShader{
	Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
	ZWrite Off
	Cull Off
	Stencil{
		Ref 250.000000
		Pass Replace
	}

	CGINCLUDE

	#include "UnityCG.cginc"

	// uniforms
	float4 _MainTex_ST;

	// vertex shader input data
	struct appdata {
		float3 pos : POSITION;
		float3 uv0 : TEXCOORD0;
	};

	// vertex-to-fragment interpolators
	struct v2f {
		fixed4 color : COLOR0;
		float2 uv0 : TEXCOORD0;
		float4 pos : SV_POSITION;
	};

	// vertex shader
	v2f vert(appdata IN) {
		v2f o;
		half4 color = half4(0,0,0,1.1);
		float3 eyePos = mul(UNITY_MATRIX_MV, float4(IN.pos,1)).xyz;
		half3 viewDir = 0.0;
		o.color = saturate(color);
		// compute texture coordinates
		o.uv0 = IN.uv0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
		// transform position
		o.pos = UnityObjectToClipPos(IN.pos);
		return o;
	}

	// textures
	sampler2D _MainTex;
	float _RangeInput;
	float _TexHeight;
	float _TexWidth;

	// fragment shader
	fixed4 frag(v2f IN) : SV_Target{
		fixed4 col;

		fixed4 tmp = tex2D(_MainTex, IN.uv0.xy);
		fixed tr = dot(tmp.rgb, float3(0.393, 0.769, 0.189));
		fixed tg = dot(tmp.rgb, float3(0.349, 0.686, 0.168));
		fixed tb = dot(tmp.rgb, float3(0.272, 0.534, 0.131));

		col = fixed4(tr, tg, tb, 1);

		return col;
	}

		
	ENDCG

	Pass {
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma target 3.0
		ENDCG
	}
	
	
}
	Fallback "Diffuse"
}