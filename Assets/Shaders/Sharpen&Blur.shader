Shader "Custom/Sharpen" {
Properties{
	_MainTex("Base (RGB)", 2D) = "white" { }
	_RangeInput("Range Input", Float) = 1
}
SubShader{
	Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
	Pass{
		Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
		ZWrite Off
		Cull Off
		Stencil{
			Ref 250.000000
			Pass Replace
		}
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma target 3.0
#include "UnityCG.cginc"



		// uniforms
		float4 _MainTex_ST;

		// vertex shader input data
		struct appdata {
			float3 pos : POSITION;
			float3 uv0 : TEXCOORD0;
		};

		// vertex-to-fragment interpolators
		struct v2f {
			fixed4 color : COLOR0;
			float2 uv0 : TEXCOORD0;
			float4 pos : SV_POSITION;
		};

		// vertex shader
		v2f vert(appdata IN) {
			v2f o;
			half4 color = half4(0,0,0,1.1);
			float3 eyePos = mul(UNITY_MATRIX_MV, float4(IN.pos,1)).xyz;
			half3 viewDir = 0.0;
			o.color = saturate(color);
			// compute texture coordinates
			o.uv0 = IN.uv0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
			// transform position
			o.pos = UnityObjectToClipPos(IN.pos);
			return o;
		}

		// textures
		sampler2D _MainTex;
		float _RangeInput;

		// fragment shader
		fixed4 frag(v2f IN) : SV_Target{
			fixed4 col;
			fixed4 tex;
			// SetTexture #0
			
			//Sharpen and Average filters with varying sample sizes
			
			float dist = abs(200 * _RangeInput - 1000);	//dist defines the 3x3 sample grid size and is dependant on the _RangeInput value
			col = tex2D(_MainTex, IN.uv0.xy);
			if (_RangeInput < 6) {	//filters 0-5 get the sharpen filter variant where in a 3x3 grid all values get * by -1 part from the center that gets * 9
				for (int i = 0; i < 9; i++) {
					tex = tex2D(_MainTex, float2(IN.uv0.x + (i % 3 - 1) / (dist + 250), IN.uv0.y + floor((i - 3) / 3) / (dist + 250)));	//This selects the texels in a 3x3 grid around the current texel, the sampple size of the 3x3 grid varies based on the dist value
					if (i == 4) col.rgb += tex.rgb * 8;
					else col.rgb -= tex.rgb;
				}
			}
			else {
				for (int i = 0; i < 9; i++) {	//filters 6-10 get the average filter variant where in a 3x3 grid all values get added together and then devided by 9
					tex = tex2D(_MainTex, float2(IN.uv0.x + (i % 3 - 1)*4 / (dist + 250), IN.uv0.y + floor((i - 3) / 3)*4 / (dist + 250)));
					if (i != 4) col.rgb += tex.rgb;
				}
				col.rgb /= 9;
			}

			return col;
		}

		// texenvs
		//! TexEnv0: 01010000 01010000 [_MainTex]
		ENDCG
	}
}


	Fallback "Diffuse"
}












