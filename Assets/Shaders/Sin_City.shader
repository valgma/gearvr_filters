Shader "Custom/Sin_City" {
Properties{
	_MainTex("Base (RGB)", 2D) = "white" { }
	_RangeInput("Range Input", Float) = 1
}
SubShader{
	Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
	Pass{
		Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
		ZWrite Off
		Cull Off
		Stencil{
			Ref 250.000000
			Pass Replace
		}
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma target 3.0
#include "UnityCG.cginc"




		///Functions block


fixed4 RGBtoHSV(in fixed4 RGB){// http://www.chilliant.com/rgb2hsv.html
	fixed4 k = fixed4(0.0, -1.0/3.0, 2.0/3.0, -1.0);
	fixed4 p = RGB.g < RGB.b ? fixed4(RGB.b, RGB.g, k.w, k.z) : fixed4(RGB.gb, k.xy);
	fixed4 q = RGB.r < p.x   ? fixed4(p.x, p.y, p.w, RGB.r) : fixed4(RGB.r, p.yzx);
	float d = q.x - min(q.w, q.y);
	float e = 1.0e-10;
	return fixed4(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x, RGB.a);
}
fixed4 HSVtoRGB(in fixed4 HSV){
	fixed4 k = fixed4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	fixed3 p = abs(frac(HSV.xxx + k.xyz) * 6.0 - k.www);
	return fixed4(HSV.z * lerp(k.xxx, clamp(p - k.xxx, 0.0, 1.0), HSV.y),HSV.a);
}
		//functions block end


		// uniforms
		float4 _MainTex_ST;

		// vertex shader input data
		struct appdata {
			float3 pos : POSITION;
			float3 uv0 : TEXCOORD0;
		};

		// vertex-to-fragment interpolators
		struct v2f {
			fixed4 color : COLOR0;
			float2 uv0 : TEXCOORD0;
			float4 pos : SV_POSITION;
		};

		// vertex shader
		v2f vert(appdata IN) {
			v2f o;
			half4 color = half4(0,0,0,1.1);
			float3 eyePos = mul(UNITY_MATRIX_MV, float4(IN.pos,1)).xyz;
			half3 viewDir = 0.0;
			o.color = saturate(color);
			// compute texture coordinates
			o.uv0 = IN.uv0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
			// transform position
			o.pos = UnityObjectToClipPos(IN.pos);
			return o;
		}

		// textures
		sampler2D _MainTex;
		float _RangeInput;

		// fragment shader
		fixed4 frag(v2f IN) : SV_Target{
			fixed4 col;
			fixed4 tex;
			fixed4 hsl, avg_col;

			
			//A filter that simulates the Sin City artistic filter, the flters sub filters have their main color variants that exclude all other colors
			
			hsl = RGBtoHSV(tex2D(_MainTex, IN.uv0.xy));
			float pi = 3.14159;
			float section = 0.1 * _RangeInput + 0.05;	//Section selects the hue section for filters 1-10
			if (section > 0) {
				if (section >= 0.8 && (hsl.x < section && hsl.x >(section - 0.8))) hsl.y -= 0.4;		//For the single color sub filters all excluded colors have their saturation rediced
				else if (section < 0.8 && (hsl.x < section || hsl.x > section + 0.2)) hsl.y -= 0.4;
				else hsl.y *= 1;
			}
			hsl.y = min(max(hsl.y, 0),1);


			hsl.z = min(pow(hsl.z + 0.05, 1+ 4 * 0.6), 1.0);			//Hand tested values to create the sin city effect in both the saturation and brightness channels
			hsl.y = min(pow(hsl.y + 0.1, 1 + 4 * 0.7), 1.0);

			col = HSVtoRGB(hsl);

			return col;
		}

		// texenvs
		//! TexEnv0: 01010000 01010000 [_MainTex]
		ENDCG
	}
}


	Fallback "Diffuse"
}












