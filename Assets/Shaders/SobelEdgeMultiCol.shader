﻿Shader "Custom/SobelEdgeMultiCol" {
Properties{
	_MainTex("Base (RGB)", 2D) = "white" { }
	_RangeInput("Range Input", Float) = 0
}
SubShader{
	Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
	//UsePass "Custom/Grayscale/GRAYSHADER"  // make it black and white

	Pass{
		Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
		ZWrite Off
		Cull Off
		Stencil{
			Ref 250.000000
			Pass Replace
		}
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma target 3.0
#include "UnityCG.cginc"

		// uniforms
		float4 _MainTex_ST;

		// vertex shader input data
		struct appdata {
			float3 pos : POSITION;
			float3 uv0 : TEXCOORD0;
		};

		// vertex-to-fragment interpolators
		struct v2f {
			fixed4 color : COLOR0;
			float2 uv0 : TEXCOORD0;
			float4 pos : SV_POSITION;
		};

		// vertex shader
		v2f vert(appdata IN) {
			v2f o;
			half4 color = half4(0,0,0,1.1);
			float3 eyePos = mul(UNITY_MATRIX_MV, float4(IN.pos,1)).xyz;
			half3 viewDir = 0.0;
			o.color = saturate(color);
			// compute texture coordinates
			o.uv0 = IN.uv0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
			// transform position
			o.pos = UnityObjectToClipPos(IN.pos);
			return o;
		}

		// textures
		sampler2D _MainTex;
		float4 _MainTex_TexelSize;
		float _RangeInput;

		// fragment shader
		float4 frag(v2f IN) : SV_Target{
			float4 col;
			float dx = (1 + _RangeInput ) *_MainTex_TexelSize.x; // _ScreenParams.x;
			float dy = (1 + _RangeInput) * _MainTex_TexelSize.y;
						
			float4 p1 = tex2D(_MainTex, float2(IN.uv0.x - dx, IN.uv0.y + dy));
			float4 p2 = tex2D(_MainTex, float2(IN.uv0.x,		 IN.uv0.y + dy));
			float4 p3 = tex2D(_MainTex, float2(IN.uv0.x + dx, IN.uv0.y + dy));
			float4 p4 = tex2D(_MainTex, float2(IN.uv0.x - dx, IN.uv0.y));
			float4 p5 = tex2D(_MainTex, float2(IN.uv0.x,		 IN.uv0.y));
			float4 p6 = tex2D(_MainTex, float2(IN.uv0.x + dx, IN.uv0.y));
			float4 p7 = tex2D(_MainTex, float2(IN.uv0.x - dx, IN.uv0.y - dy));
			float4 p8 = tex2D(_MainTex, float2(IN.uv0.x,		 IN.uv0.y - dy));
			float4 p9 = tex2D(_MainTex, float2(IN.uv0.x + dx, IN.uv0.y - dy));

			float4 g = abs(p1 + 2 * p2 + p3 - p7 - 2 * p8 - p9) + abs(p3 + 2*p6 + p9 - p1 -2*p4 - p7);
			g = clamp(g, 0, 1);
			//col = float4(g, g, g, 1);
			col = g;

			return col;
		}

		// texenvs
		//! TexEnv0: 01010000 01010000 [_MainTex]
		ENDCG
	}
}
	Fallback "Diffuse"
}