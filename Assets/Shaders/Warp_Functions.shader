Shader "Custom/Warp" {
Properties{
	_MainTex("Base (RGB)", 2D) = "white" { }
	_RangeInput("Range Input", Float) = 1
}
SubShader{
	Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
	Pass{
		Tags{ "QUEUE" = "geometry-11" "RenderType" = "Opaque" }
		ZWrite Off
		Cull Off
		Stencil{
			Ref 250.000000
			Pass Replace
		}
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma target 3.0
#include "UnityCG.cginc"


		// uniforms
		float4 _MainTex_ST;

		// vertex shader input data
		struct appdata {
			float3 pos : POSITION;
			float3 uv0 : TEXCOORD0;
		};

		// vertex-to-fragment interpolators
		struct v2f {
			fixed4 color : COLOR0;
			float2 uv0 : TEXCOORD0;

			float4 pos : SV_POSITION;
		};

		// vertex shader
		v2f vert(appdata IN) {
			v2f o;
			half4 color = half4(0,0,0,1.1);
			float3 eyePos = mul(UNITY_MATRIX_MV, float4(IN.pos,1)).xyz;
			half3 viewDir = 0.0;
			o.color = saturate(color);
			// compute texture coordinates
			o.uv0 = IN.uv0.xy * _MainTex_ST.xy + _MainTex_ST.zw;

			// transform position
			o.pos = UnityObjectToClipPos(IN.pos);
			return o;
		}

		// textures
		sampler2D _MainTex;
		float _RangeInput;
		// fragment shader
		fixed4 frag(v2f IN) : SV_Target{
			fixed4 col;
			fixed4 tex;
			// SetTexture #0

			float tc = _CosTime.w;
			float ts = _SinTime.w;

			float2 anchor = { 0.39 , 0.47 };
			float fun = 0;
			float pi = 3.14159;
			
			
			//This filter creates a warping effect around the center of the image

			float dist = min(pow(pow(IN.uv0.x - anchor[0], 2) + pow(IN.uv0.y - anchor[1], 2), 0.35) / (0.5), 1);		//dist functions define the distance from the center of the image(anchor) with a cap of 1
			if (_RangeInput == 2) fun = -abs(dist - 0.5) * 2 +1;			// the functions define the warping effect type
			if (_RangeInput == 4) fun = (dist-0.5)*1.5;						
			if (_RangeInput == 5) fun = sin(dist * pi * 2);
			if (_RangeInput == 6) fun = -cos(dist * pi)/2;

			dist = min(pow(pow(IN.uv0.x - anchor[0], 2) + pow(IN.uv0.y - anchor[1], 2), 0.55) / (0.5), 1);
			if (_RangeInput == 1) fun = abs(dist - 0.5);
			if (_RangeInput == 3) fun = pow(dist+0.3, 3) - 1;
			if (_RangeInput == 7) fun = 1 / (1 + pow(2.7182, (dist - 0.5) * 5 * (2)));
			if (_RangeInput == 8) fun = tan(dist * pi * 2);
			if (_RangeInput == 9) fun = abs(sin(dist * pi * 3));
			if (_RangeInput == 10) fun = floor(sin(dist * pi * 6)) / 2 + 0.5;

			float xc = min(IN.uv0.x - (IN.uv0.x - anchor[0])*fun, 0.78);			//A bit from the edges get cut off since the area is black
			float yc = min(IN.uv0.y - (IN.uv0.y - anchor[1])*fun, 0.87);
			


			col = tex2D(_MainTex, float2(xc, yc));

			return col;
		}

		// texenvs
		//! TexEnv0: 01010000 01010000 [_MainTex]
		ENDCG
	}
}


	Fallback "Diffuse"
}












