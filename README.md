# Filters for GearVR

The goal of the project is to create a number of post processing filters for use with Samsung GearVR on live camera picture. So that you could "see" the world through the filter. We will capture the camera image, apply a filter and display the result. As the GearVR blocks all outside view, the viewer will experience the different world (although in 2D).

The project is being implemented as a course project for Computer Graphics ([link](https://courses.cs.ut.ee/2016/cg/fall)). Also some other [*iCV*](http://icv.tuit.ut.ee/) members will contribute with some filters.

We use [Unity](https://unity3d.com/legal), [Oculus](https://developer3.oculus.com/downloads/game-engines/1.9.0/Oculus_Utilities_for_Unity_5/) and [Vuforia](https://developer.vuforia.com/legal/tos). Please respect all the licences and requirements they pose.

## Team
### Computer Graphics team
* Egils Avots
* Rain Eric Haamer
* Lembit Valgma
### Other contributors
* Iiris L�si
* Taisi Telve

## Tutorials
Check the [Wiki page](https://bitbucket.org/valgma92/gearvr_filters/wiki/Home)

## Creating filters
If you want to implement a filter, please choose one from the Issue page and assign it to yourself. Once you're ready, test the filter!!! If it's a shader, then upload it to the Shader folder. Then mark issue closed.